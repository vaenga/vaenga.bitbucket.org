<?php

if (!defined('PBOX')) { header('HTTP/1.0 404 Not Found'); die(); };

global $now;

$now = time();

function past($str)	
{
	global $now;
	list($date, $time) = explode(' ', $str);
	list($date1, $date2, $date3) = explode('.', $date);
	list($time1, $time2) = explode(':', $time);				
	$time = mktime((int)$time1, (int)$time2, 0, (int)$date2, (int)$date1, (int)$date3);			
	return $now >= $time;
}
	
global $db;
$cache = array();
if(0) $db = new SQLite3(dirname(__FILE__).'/data.db');		

function getNline($n)
{	
	global $cache;	
	if(isset($cache[$n])) 
		return $cache[$n]; //use cache

	if(0)
	{
	  global $db;
	  $statement = $db->prepare("SELECT * FROM delayed_links where id = $n");
	  $result = $statement->execute();
	  $row = $result->fetchArray();
	  if($row == false) { header('HTTP/1.0 404 Not Found'); die(); }
	  $cache[$n] = $row; //store to cache
	  return $row;		
	}
	
	$counter = -1;
	$fp = fopen(dirname(__FILE__)."/delayeddump.php", "r");
	$line = '';
	while (($line = fgets($fp)) !== false) {
		if(++$counter == $n) break;
	}
	fclose($fp);  
	$cache[$n] = $line; //store to cache  
	return $line;
}

function showitem($n)
{	
	if(0)
	{
		global $now;
		$row = getNline($n);
		$today = (double)date('YmdHi', $now);
		return $today >= $row['date'];
	}
	$line = getNline($n);
	$item = explode('|', $line);
	return past($item[0]);
}

function showcomment($n, $cn)
{
	if(0)
	{
		global $now;
		$row = getNline($n);
		$dates = explode(',', $row['comments']);
		$today = (double)date('YmdHi', $now);
		return $today >= $dates[$cn];
	}
	$line = getNline($n);
	$item = explode('|', $line);
	$dates = explode(',', $item[2]);	
	return past($dates[$cn]);
}

function upfirst($str) {
	if(!function_exists('mb_substr')) return $str;
	return mb_strtoupper(mb_substr($str, 0, 1)) . mb_substr($str, 1);
}

function showmap($split, $page, $size, $map, $ext, $separator)
{
	$mapitems = array();
	if(0)
	{
		global $now;
		global $db;
		
		if($split)
		{
			$skip = ($page-1) * $size;
			$statement = $db->prepare("SELECT link FROM delayed_links WHERE date < " . date('YmdHi', $now) . " ORDER BY id LIMIT $skip, $size");
		}
		else
			$statement = $db->prepare("SELECT link FROM delayed_links WHERE date < " . date('YmdHi', $now) . " ORDER BY id");		
		
		$result = $statement->execute();
		while($row = $result->fetchArray())
		{
			$item = $row['link'];
			if (0) $item = iconv('UTF-8', 'WINDOWS-1251', $item);
			$achorPos = strpos($item, '>') + 1;
			$mapitems[] = substr($item, 0, $achorPos) . upfirst(substr($item, $achorPos));
		}
	}	
	else
	{	
		$fp = fopen(dirname(__FILE__)."/delayeddump.php", "r");
		$line = fgets($fp);
		while (($line = fgets($fp)) !== false) {
			$item = explode('|', $line);
			if(past($item[0])) 
			{
				$item = $item[1];
				$achorPos = strpos($item, '>') + 1;
				$mapitems[] = substr($item, 0, $achorPos) . upfirst(substr($item, $achorPos));
			}
			else	
				break;
		}
		fclose($fp);
	}
	
	if(0)	
	{
		echo "<ul><li>".join("</li><li>",$mapitems)."</li></ul>";
		
		if($split)
		{
			$statement = $db->prepare("SELECT count(*) FROM delayed_links WHERE date < " . date('YmdHi', $now) . " ORDER BY id");
			$result = $statement->execute();
			$row = $result->fetchArray();
			$count = $row[0]/$size;
			for($i=0;$i<$count;$i++)
			{
				$j=$i+1;
				if($j==$page)
					echo "<b>$j</b>";
				else
					echo "<a href='$map$j$ext'>$j</a>";
				if($i!=$count-1)echo $separator;
			}
		}
	}
	else
	{
		if($split)
		{
			$mapchunks = array_chunk($mapitems, $size);
			$mapitems = $mapchunks[$page-1];
		}
		
		echo "<ul><li>".join("</li><li>",$mapitems)."</li></ul>";
		
		if($split)
		{
			$count = count($mapchunks);
			for($i=0;$i<$count;$i++)
			{
				$j=$i+1;
				if($j==$page)
					echo "<b>$j</b>";
				else
					echo "<a href='$map$j$ext'>$j</a>";
				if($i!=$count-1)echo $separator;
			}
		}	
	}
}				

function tags($size)
{
	$pastitems = array();
	if(0)
	{
		global $now;
		global $db;
		$statement = $db->prepare("SELECT link FROM (SELECT id,link FROM delayed_links WHERE date < " . 
			date('YmdHi', $now) . " ORDER BY id DESC LIMIT $size) ORDER BY id ASC");
		$result = $statement->execute();
		while($row = $result->fetchArray())
		{
			$item = $row['link'];
			if (0) $item = iconv('UTF-8', 'WINDOWS-1251', $item);
			$pastitems[] = $item;
		}
	}	
	else
	{
		$fp = fopen(dirname(__FILE__)."/delayeddump.php", "r");
		$line = fgets($fp);
		while (($line = fgets($fp)) !== false) {
			$item = explode('|', $line);
			if(past($item[0])) 
				$pastitems[] = $item[1];
			else	
				break;
		}
		fclose($fp);
	}
		
	$pcount = sizeof($pastitems);
	$md5 = md5($pcount);		
	$md5 = $md5.$md5.$md5.$md5.$md5;
	
	$cnt=0;
	$ids = array();
	for($i=0;$i<strlen($md5);$i++)
	{	
		$rn = ord($md5[$i]);
		$id = $rn % sizeof($pastitems);
		if(isset($ids[$id]))continue;
		$cnt++;
		if($cnt>=$size)break;
		$ids[$id] = $id;		
		$ritem = $pastitems[$id];
		$s=($id*10 % 17)+8;
		echo str_replace("<a ",'<a class="tag" style="font-size:'.$s.'pt" ',$ritem).'&nbsp; ';
	}
}

function checkpost($id)
{
  if (!showitem($id))
  { 
    header('HTTP/1.0 404 Not Found'); 
    die(); 
  }
}

?>